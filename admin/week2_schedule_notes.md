
# Topic Details
Need to resolve what will be covered in lecture, hands-on tutorial and workshop

## Amplicon Bioinformatics
1. Demux
2. DADA2
3. Filtering (brief)
    - Taxa: rare taxa, low abundance taxa
    - Samples
4. Contamination (brief)
    - Negative controls
    - decontamination:  [decontam](https://benjjneb.github.io/decontam/vignettes/decontam_intro.html)
5. Batch Effects (brief)
    - avoiding
    - mention tools (but no details)
        - https://microbiomejournal.biomedcentral.com/articles/10.1186/s40168-020-00998-4
6. Downloading Data (very brief, Appendix)

7. Building pylogenetic trees (so Pixu can use them)



## Descriptive Statistics
1. Alpha diversity
2. Ordination: PCoA
3. Beta diversity
4. Transformations
    - additive log-ratio
    - central log-ratio
    - PhILR?
	
## Differential Analysis
1. Global: Permanova
2. Biomarker Discovery: ANSCOMBC
3. Regression
    - compositional covriate regression
    - Aitchison
4. Correlation? Covarying Taxa?
    - https://journals.sagepub.com/doi/full/10.1177/1535370219836771
    - ?? https://assets.researchsquare.com/files/rs-1497744/v1_covered.pdf?c=1648833348

## Shotgun Metagenomics
1. Bioinformatics processing to counts (Kaiju, Kraken, or Metaphlan)
    - Kraken2
    - Bracken [website](http://ccb.jhu.edu/software/bracken/), [paper](https://doi.org/10.7717/peerj-cs.104), [github](https://github.com/jenniferlu717/Bracken)
        - need to discuss probabilistic assignment to lower taxonomic levels
2. Metabolic pathway analysis??? HumAnN????
3. Gene level analysis???

## Appendix
These topics will not be covered in class or will be covered very briefly, but we will provide tutorials in an

### Downloading Data
- SRA
- Duke Sequencing Core
- Argonne?
