---
output:
  word_document: default
  html_document: default
---
The course will consist of a mix of lecture and hands-on data analysis sessions. Below is preliminary summary of topics that we expect to cover, although this is subject to change.

# Week 1 Topics
	- Introduction to RStudio
	- Introduction to Reproducible Analysis
	- Mechanics of High Throughput Sequencing Technology
	- Introduction to UNIX
	- Introduction to R
	- Microbiome Experimental Methods
	- Introduction to Cancer Immunology
	- Statistics Background
	- Application of Relevant R packages
		- tidyr
		- ggplot2
		- dplyr
		- phyloseq

# Week 2 Topics
	- Amplicon Bioinformatics
		- Demultiplexing
		- DADA2
		- Filtering Taxa and Samples
		- Negative controls and decontamination
		- Phylogenetic trees
	- Descriptive Statistics
		- Alpha diversity
		- Ordination
		- Beta diversity
		- Transformations
	- Differential Analysis
		- Global (Permanova)
		- Biomarker Discovery (ANSCOMBC)
		- Regression: Aitchison & Compositional Covariate Regression
    - Shotgun Metagenomics Bioinformatics
    - Gene level and/or Pathway Analysis
