| Day       |        Time | Topic                               | Links                                      | Instructor | Backup |
| --------- | ----------- | ----------------------------------- | ------------------------------------------ | ---------- | ------ |
| Monday    | 09:00-10:15 | Amplicon Bioinformatics             |                                            | Granek     | Owzar  |
| 5/22/2023 | 10:30-11:45 | Amplicon Bioinformatics             |                                            | Granek     | Owzar  |
|           | 13:15-14:30 | Tutorial                            | [TOC](../content/amplicon/amplicon_toc.md) | Granek     | Owzar  |
|           | 14:45-16:00 | Workshop                            |                                            | Granek     | Owzar  |
|           |             |                                     |                                            |            |        |
| Tuesday   | 09:00-10:15 | Descriptive Statistics              |                                            | Shi        | Ji     |
| 5/23/2023 | 10:30-11:45 |                                     |                                            | Shi        | Ji     |
|           | 13:15-14:30 | Tutorial                            |                                            | Shi        | Ji     |
|           | 14:45-16:00 | Workshop                            |                                            | Shi        | Ji     |
|           |             |                                     |                                            |            |        |
| Wednesday | 09:00-10:15 | Differential Analysis               |                                            | Shi        | Xie    |
| 5/24/2023 | 10:30-11:45 |                                     |                                            | Shi        | Xie    |
|           | 13:15-14:30 | Tutorial                            |                                            | Shi        | Owzar  |
|           | 14:45-16:00 | Workshop                            |                                            | Shi        | Owzar  |
|           |             |                                     |                                            |            |        |
| Thursday  | 09:00-10:15 | Shotgun Metagenomics Bioinformatics |                                            | Granek     | Owzar  |
| 5/25/2023 | 10:30-11:45 |                                     |                                            | Granek     | Owzar  |
|           | 13:15-14:30 | Tutorial                            |                                            | Granek     | Owzar  |
|           | 14:45-16:00 | Workshop                            |                                            | Granek     | Owzar  |
|           |             |                                     |                                            |            |        |
| Friday    | 09:00-10:15 | Gene level and/or Pathway Analysis  |                                            | Granek     | Xie    |
| 5/26/2023 | 10:30-11:45 |                                     |                                            | Shi        | Xie    |
|           | 13:15-14:30 | Tutorial                            |                                            | Granek/Shi | Owzar  |
|           | 14:45-16:00 | Workshop                            |                                            | Granek/Shi | Owzar  |
