
# Primary Publications
## Matson (2018)
   - Paper: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6707353/
   - Data Characteristics
	 - 39 patients with matched 16S and Shotgun Metagenomic data
	 - 2x150 paired-end, average of 80.4 million reads/sample (ranging: 38.9 to 156.7 million)
	 - 91 FMT mice with 16S only
   - Data Accessibility
	 - [PRJNA399742](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA399742): All data (human 16S and shotgun, mouse 16S)
   - Misc
	 - See Fig S1, https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6707353/bin/NIHMS1043756-supplement-Supplemental.pdf

### Pro
- SRA data seems more compliant
- more matched subjects

### Con
- no demographic data (age or sex)
- 16S reads are 151PE

## Gopalakrishnan (2017)
   - Paper: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5827966/
   - Data Characteristics
	 - 25 patients with matched 16S and Shotgun Metagenomic data
	 - Also 16S oral samples
	 - Mouse 16S samples from FMT
   - Data Accessibility
     - [PRJEB22894](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB22894): Human fecal 16S (multiplexed?)
     - [PRJEB22874](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB22874): Human oral 16S (multiplexed?)
     - [PRJEB22895](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB22895): Mouse ?fecal? 16S (multiplexed?) [12 FMT mice]
     - [PRJEB22893](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB22893): Human fecal shotgun

### Pro
- demographic data
### Con
- SRA submission is weird (16S data seems multiplexed)
- *16S data appears to be partially processed*: trimmed, phred scores modified (mostly "J"), and possibly merged R1 & R2
  - This is a deal killer for me!!

# Meta analysis
## Shaikh (2022)

A uniform computational approach improved on existing pipelines to reveal microbiome biomarkers of non-response to immune checkpoint inhibitors
   - Reanalized data from 5 papers
   - PMC: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9053858/
   - summary of papers: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9053858/table/T1/
   - summary of samples: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9053858/bin/NIHMS1676592-supplement-2.xls

### Raw Data

[Table S1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9053858/bin/NIHMS1676592-supplement-2.xls) has the SRR accession numbers and metadata for all data used. NOTE: you need to scroll to see column A, which has accession numbers 


Raw sequencing data was retrieved with the following accession numbers: 

| Paper          | Bioproject                                    |
|----------------|---------------------------------------------- |
| Gopalakrishnan | PRJEB22894, PRJEB22874 PRJEB22895, PRJEB22893 |
| Matson         | PRJNA399742                                   |
| Chaput         | PRJNA379764                                   |
| Frankel        | PRJNA397906                                   |
| Routy          | PRJEB22863                                    |
| Peters         | PRJNA541981                                   |
| Zheng          | PRJNA505228                                   |
| Hakozaki       | PRJNA606061                                   |



### Features of Meta-anaylsis

#### Longitudinal data
"Two cohorts, Chaput (n=23) (9) and Routy (n=44) (6), reported on paired samples prior to and during treatment (up to 2 months after initial of ICI treatment)."

#### Impact of antibiotic use on alpha diversity
