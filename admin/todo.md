# General Computing
- [X] SLURM setup (sysadmin)
- [X] update paths in config.R [Josh]
- [X] Download Matson data [Josh]

# Course Image
- [X] update Kaiju
- [ ] Kraken
  - [ ] Kraken Tools
  - [ ] Kraken


# Amplicon
- [X] move DADA2 references and path [Josh]
- [X] write script to subset Matson 16S data for fast dada2 runtime (aim for <5 minutes with 40 cores)[Marissa]
- [X] modify existing notebooks in `content/amplicon` to run Matson data
	- [X] dada2_tutorial_1_6.Rmd
- [X] generate phylogenetic tree and add to phyloseq object: look at adapting [../content/amplicon/biostats/phylogenetic_trees.Rmd] [Marissa]
  - [X] make it more literate [Marissa]
- [ ] run decontam? [Marissa]

# Shotgun
- [ ] Need to cover FASTQ QC
- [ ] subset Matson shotgun data
- [ ] ??? Download references?
  - [ ] ??? script to download pre-built Kaiju reference
  - [ ] ??? build Kaiju database?
- [ ] Clean host contamination (maybe for Appendix)
  - [ ] Kneaddata?
  - [ ] check how NCBI cleans human DNA from SRA submissions
  - [ ] incoporate human data in Kaiju DB??
- [X] trim and filter reads [Xiaodi]
  - [X] find year 1 Trimmotmatic notebooks as a starting point [Josh]
- [ ] Run Kaiju on Matson shotgun data (subset)
- [ ] Script Download Kaiju databases [Josh - in process]
  - [X] build refseq DB with eukaryotes? [xiaodi]
    - https://github.com/bioinformatics-centre/kaiju/issues/200
    - https://ftp.ncbi.nlm.nih.gov/genomes/refseq/
    - https://ftp.ncbi.nlm.nih.gov/genomes/refseq/README.txt
- [X] Figure out how to import Kaiju into phyloseq
  - [X] import counts
  - [X] import tax assignments
- [X] Figure out multiple samples in Kaiju
  - FOR loop?
  - multiple samples in single kaiju run?
- [X] Figure out what this means: "cannot be assigned to a (non-viral) species"
- [X] run fastqc+multiqc [Xiaodi]
  - [X] find year 1 notebooks as a starting point [Josh]
- [X] Run Kaiju on full Matson shotgun data
- [ ] Make phyloseq object from Kaiju output [Marissa]
- [ ] Look into making a tree from Kaiju output [Marissa]
- [ ] Finalize Rmds
  - [ ] Organize Rmds
  - [ ] Clean up Rmds
  - [ ] Annotate Rmds
- [ ] Kaiju Alternatives
  - [ ] Kraken2/Bracken
  - [ ] [KrakenUniq](https://github.com/fbreitwieser/krakenuniq)

## Pathway analysis
  1. [ ] Carnelian [Paper](https://pubmed.ncbi.nlm.nih.gov/32093762/), [Github](https://github.com/snz20/carnelian/), [Webpage](https://cb.csail.mit.edu/cb/carnelian/)
    1. [ ] Install
    2. [ ] Run test data (if available)
    3. [ ] Run Matson data
  2. [ ] Fun4Me [Paper](https://pubmed.ncbi.nlm.nih.gov/28451969/), [Software](https://sourceforge.net/projects/fun4me)
    1. [ ] Install
    2. [ ] Run test data (if available)
    3. [ ] Run Matson data
  3. [ ] HUMAnN [Software](https://github.com/biobakery/biobakery/wiki/humann3)
    1. [ ] Install
    2. [ ] Run test data (if available)
    3. [ ] Run Matson data
  3. MAG-based? (in progress)
  4. [Seq2Fun](https://www.seq2fun.ca/)??????

# Lectures
## Optional material
  - Bash for loops
  - decontam

## R Topics
- [ ] Tidyverse based on Kaiju -> phyloseq
- [ ] RDS
- [ ] ggplot dada2 follow reads through pipeline
- [ ] Phyloseq deepdive?

# Finalizing Rmds
For each final Rmd to be used in class need to do the following:
  1. XXXXXXXX
- 

# Helpful resources
- [instructor_info.md](instructor_info.md)
- [ondemand_howto.md](ondemand_howto.md)
