Shortened URL: https://duke.is/2tymv

# Schedule
- [Week 1 Schedule](admin/week1_schedule.md)
- Week 2 Schedule

# Computing Environments
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
- [RStudio on DCC OnDemand](computing/dcc_ood/dcc_ood_rstudio.md)

# Workshop Content
- [Initial download of workshop content](computing/reproducible/git_cloning.md)
- [Update workshop content](computing/reproducible/git_pull.md)

# Miscellaneous
- [Managing Notes](admin/managing_file_modifications.md)
- After the MIC Course
