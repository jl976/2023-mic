# sample summary

library(readr)
library(dplyr)
library(fs)
library(here)
library(tidyverse)

"content/config.R" %>%
  here() %>%
  source()

md <- read_csv(matson_sample_csv) 
md

colnames(md)
md$DataType

md %>%
  group_by(DataType) %>%
  summarize(n = n())

md %>%
  group_by(PatientID, DataType) %>%
  summarize(n = n()) %>%
  pivot_wider(id_cols = PatientID,
              names_from= DataType,
              values_from =n) %>%
  mutate(both.datatypes = `16SrRNA`+WGS) -> out

out %>%
  filter(!is.na(both.datatypes)) %>%
  pull(PatientID) %>%
  length()

nrow(md)
length(unique(md$PatientID))


