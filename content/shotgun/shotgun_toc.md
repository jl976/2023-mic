Shotgun TOC

## Appendix
1. [Configuration File](../config.R)
2. [Download FASTQs](../prep/download_matson_data.Rmd)
3. [Download Taxonomic References](prep/download_dada_references.Rmd)


4. [Kraken2 to Bracken]() -- To be set up by XQ
5. [Assign NCBI taxon IDs phylogenetic tree](prep/XX_assign-taxa-to-tree.Rmd)
6. [Create phyloseq objects](prep/XX_make-phyloseq.Rmd)

