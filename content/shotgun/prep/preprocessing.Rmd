---
output:
  md_document:
    variant: markdown_github
    toc: true
  html_document:
    toc: true
    toc_float: true
---

# Goals

- Produce quality reports for multiple samples using FastQC
- Combine multiple FastQC reports using MultiQC
- Trim low-quality reads and adapters using Trimmomatic
- Perform post-trimming QC

# Setup

First we load R libraries,

```{r}
library(tidyverse)
library(here)
```

Then we load the paths and directories pre-defined in the `config.R`,

```{r}
"content/config.R" %>%
    here() %>%
    source()
```

We also need to define some shell variables for later use in the bash chunks, using R function `Sys.setenv`,

```{r}
# Input
Sys.setenv(
  RAW_FASTQ_DIR = shotgun_data_dir,
  N_THREADS = 20
)
# Output
Sys.setenv(
  OUT_DIR = shotgun_out_dir,
  FASTQC_INIT_DIR = shotgun_fastqc_init_dir,
  MULTIQC_INIT_DIR = shotgun_multiqc_init_dir,
  TRIM_DIR = shotgun_trim_dir,
  FASTQC_TRIM_DIR = shotgun_fastqc_trim_dir,
  MULTIQC_TRIM_DIR = shotgun_multiqc_trim_dir
)
```

Let's check the values of the path variables,

```{bash}
echo $RAW_FASTQ_DIR
echo $N_THREADS
echo $OUT_DIR
echo $FASTQC_INIT_DIR
echo $MULTIQC_INIT_DIR
echo $TRIM_DIR
echo $FASTQC_TRIM_DIR
echo $MULTIQC_TRIM_DIR
```
And make the output directories, the flag `-p` is to suppress the error if the directory's already existed and make parent directories as needed. 

```{bash}
mkdir -p $FASTQC_INIT_DIR
mkdir -p $MULTIQC_INIT_DIR
mkdir -p $TRIM_DIR
mkdir -p $FASTQC_TRIM_DIR
mkdir -p $MULTIQC_TRIM_DIR
```

Let's check to make sure the directories are properly created. We will run `ls` and check that these directories now exist in the `$OUT_DIR` directory.

```{bash}
ls -l $OUT_DIR
```

# Initial QC

Let's run some basic analysis using a program called `FastQC`. This shouldn't take too long.

If you need to know how to run a program, most unix programs will give you some information about themselves if you run with a `-h` or `--help` command line flag

```{bash}
fastqc -h
```

Let's pick a file to run on. Since we don't want to wait too long for it to run, let's pick the smallest file for now. The flag `-lSrh` specifies multiple single-character options that will enable the `ls` to show the files in the `$RAW_FASTQ_DIR` directory in a long listing format (`-l`), rank them by file sizes (`-S`) in a reversed (ascending) order (`-r`), and print out the file sizes in a human-readable format (`-h`).

```{bash}
ls -lSrh $RAW_FASTQ_DIR
```

Some key options to specify for FastQC are,

- `--threads N` tells FastQC to run using `N` CPU cores, which will speed things.
- `--outdir DIR` tells FastQC to output the results to the directory refered to in the `DIR` variable. 
- `--extract` keeps the output files unzipped.

We can process fastq files under the `${RAW_FASTQ_DIR}` directory all at once, using proper wildcard characters to expand the file paths (e.g., `${RAW_FASTQ_DIR}/*`), or run only 1 file at a time. Here, we're going to run fastq files in pairs through FastQC, and save the output files of each pair under a directory named by sample ID. Let's take the sample with the smallest pair of fastq files as an example,

```{bash}
ID=SRR6000890
mkdir -p ${FASTQC_INIT_DIR}/${ID}
fastqc \
  --threads ${N_THREADS} \
  --extract \
  --outdir ${FASTQC_INIT_DIR}/${ID} \
  ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
  ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz
```

Once FastQC is done running we can view the results in:

```{bash}
ID=SRR6000890
ls ${FASTQC_INIT_DIR}/${ID}
```
```{bash}
ID=SRR6000890
cp ${FASTQC_INIT_DIR}/${ID}/${ID}_1_fastqc.html ~/${ID}_1_fastqc_init.html
open ${RS_SERVER_URL}/files/${ID}_1_fastqc_init.html
```

We can use for loop to run all pairs of files through FastQC. This can take some time, we can use the saved output in `r shotgun_fastqc_init_dir`,

```{bash eval=F}
for FILENAME in ${RAW_FASTQ_DIR}/*_1.fastq.gz ; do
  ID=$(basename ${FILENAME} _1.fastq.gz)
  echo "Processing ${ID}"
  fastqc \
    --threads ${N_THREADS} \
    --extract \
    --outdir ${FASTQC_INIT_DIR}/${ID} \
    ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
    ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz > ${FASTQC_INIT_DIR}/${ID}/${ID}.log 2>&1
done
```


# Initial MultiQC

FastQC is a useful tool, but it has one problem: it generates one report for each FASTQ file.  When you have more than a handful of FASTQs (as most projects will), it is tedious to look at each one, and there is no simple way to compare them.

MultiQC is a solution to this problem. It mines the results from FastQC (and other HTS analysis tools) and generates a report that summarizes results for all the FASTQs analyzed.


Let's see how MultiQC can help us look at results from multiple FASTQ files.

```{bash}
multiqc -h
```

```{bash}
multiqc ${FASTQC_INIT_DIR} --outdir ${MULTIQC_INIT_DIR}
```

Once multiqc is done running we can view the results using the web browser, it should be in a file named `multiqc_report.html` in `r shotgun_multiqc_init_dir`:

```{bash}
ls -l ${MULTIQC_INIT_DIR}
```

```{bash}
cp ${MULTIQC_INIT_DIR}/${ID}/multiqc_report.html ~/multiqc_report_init.html
open ${RS_SERVER_URL}/files/multiqc_report_init.html
```


# Trimming and Filtering

Now we get into some actual preprocessing.  We will use **Trimmomatic** to trim adapter from our reads and do some quality filtering.  We need to trim adapter, because if a fragment is short enough, we will sequence all the way through the fragment and into the adapter.  Obviously the adapter sequence in not found in the genome, and can keep the read from aligning properly.

Because Trimmomatic is written in Java, the details of how you run it depends on how it was installed, so on other computers you might find the command to run Trimmomatic is slightly different. More details about installation and usage can be found on the [Trimmomatic GitHub Page](https://github.com/usadellab/Trimmomatic). There is also a [Trimmomatic Manual](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf) for a slightly older version.

## Basic Usage

On DCC, we can use the `TrimmomaticSE` command for single-end data and `TrimmomaticPE` for paired-end data. Let's run `TrimmomaticSE` and `TrimmomaticPE` without any arguments to get details and compare.

```{bash}
TrimmomaticSE
```

```{bash}
TrimmomaticPE
```

The big differences are:
1. **We need to supply both FASTQs in the pair.** It is **very** important to filter and trim the forward and reverse FASTQs simultaneously. If we tried to filter and trim them separately, we are likely to have some cases where a read is filtered from the forward FASTQ because it fails some quality threshold, but its paired read in the reverse FASTQ passes, so it is maintained (and vice-versa). When only one read in a pair is filtered, the FASTQs get out of sync. Some bioinformatics software will detect this, but not all. Out-of-sync FASTQs will generally produce faulty results.
2. **We need give TrimmomaticPE filenames for unpaired reads** When TrimmomaticPE does filter out a read, instead of just "throwing out" the other read in the pair, it saves it in an "unpaired" file. It saves unpaired reads from the forward FASTQ in an "unpaired forward" file and unpaired reads from the reverse FASTQ in an "unpaired reverse".
3. **Need adapter file with reverse read adapter** We will use *TruSeq2-PE.fa*
4. we can add two more parameters to the **ILLUMINACLIP** argument

## Choosing an adapter file

As part of trimming and filtering we want to remove adapter contamination. To do this we need to make a file containing the adapter sequences that we can supply to our trimming software.

The first step is find out what the adapter sequences are! To do this, we need to know what adapters were used to make our libraries. If we made the libraries ourselves, then we should know. If the libraries were made by a sequencing core, then it is best to get this information from them at the time of sequencing. Sometimes the core can give us the adapter sequences and sometimes they will just tell us the kit information and we need to look up the sequences ourselves.

FastQC can also help us infer the adapter type of our data. In our case, the [paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6707353/) links to [Supplementary Methods](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6707353/bin/NIHMS1043756-supplement-Supplemental.pdf) which tells us that the Kapa Biosystems qPCR library quantification Kit was used for library prep. We can get the [adapter sequences from Illumina](https://support-docs.illumina.com/SHARE/AdapterSeq/Content/SHARE/AdapterSeq/TruSeq/CDIndexes.htm) and make our own adapter file, but we are in luck, because Trimmomatic comes with a set of Illumina adapter files (https://github.com/timflutre/trimmomatic/tree/master/adapters).

```{bash}
cat /usr/share/trimmomatic/TruSeq2-PE.fa
```


## Running Trimmomatic

We are mostly going to use the Trimmomatic recommended parameters, because they seem reasonable.

- `-threads`: the number of threads (i.e. CPU cores) to use
- `-phred33`: tell Trimmomatic that our quality scores use the phred33 encoding
- `${ID}_1.fastq.gz`: forward read input FASTQ 
- `${ID}_2.fastq.gz`: reverse read input FASTQ 
- `${ID}_1_trim_paired.fastq.gz`: name for trimmed forward FASTQ
- `${ID}_1_trim_unpaired.fastq.gz`: name for upaired (trimmed) forward FASTQ
- `${ID}_2_trim_paired.fastq.gz`: name for trimmed reverse FASTQ
- `${ID}_2_trim_unpaired.fastq.gz`: name for upaired (trimmed) forward FASTQ
- `ILLUMINACLIP`:
    - `TruSeq2-PE.fa`: adapter file  (Trimmomatic will look in its adapter file database if it can't find this at the path specified)
    - `2`: specifies the maximum mismatch count which will still allow a full match to be performed
    - `30`: specifies how accurate the match between the two 'adapter ligated' reads must be for PE palindrome read alignment
    - `10`: specifies how accurate the match between any adapter etc. sequence must be against a read.
    - `2`: minimum length of adapter match for palindrome mode
    - `True`: even if palindrome mode detects adapter, keep both forward and reverse reads
- `LEADING:3`: quality trimming at 5' end of read, remove all bases below this threshold (phred < 3), and other bases 5' of it
- `TRAILING:3`: quality trimming at 3' end of read, remove all bases below this threshold (phred < 3), and other bases 3' of it
- `SLIDINGWINDOW:4:15`: Scan the read with a 4-base wide sliding window, cutting when the average quality per base drops below 15
- `MINLEN:36`: throw out any reads that are shorter than this (36bp) after trimming

```{bash}
ID=SRR6000890
ADAPTER=TruSeq2-PE.fa
mkdir -p ${TRIM_DIR}/${ID}
time TrimmomaticPE \
  -threads $N_THREADS \
  -phred33 \
  ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
  ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz \
  ${TRIM_DIR}/${ID}/${ID}_R1_trim_paired.fastq.gz \
  ${TRIM_DIR}/${ID}/${ID}_R1_trim_unpaired.fastq.gz \
  ${TRIM_DIR}/${ID}/${ID}_R2_trim_paired.fastq.gz \
  ${TRIM_DIR}/${ID}/${ID}_R2_trim_unpaired.fastq.gz \
  -trimlog ${TRIM_DIR}/${ID}/${ID}_trim.log \
  ILLUMINACLIP:${ADAPTER}:2:30:10:2:True \
  LEADING:3 TRAILING:3 MINLEN:36 \
  > ${TRIM_DIR}/${ID}/${ID}_trim.runinfo 2>&1
```

```{bash}
ID=SRR6000890
ls -lh ${TRIM_DIR}/${ID}
```
```{bash}
ID=SRR6000890
cat ${TRIM_DIR}/${ID}/${ID}_trim.runinfo
```

We can use the following bash chunk to trim all samples, but it can take some time. We can use the saved output in `r shotgun_trim_dir`,

```{bash eval=F}
ADAPTER=TruSeq2-PE.fa  

for FILENAME in ${RAW_FASTQ_DIR}/*_1.fastq.gz ; do
  ID=$(basename $FILENAME _1.fastq.gz)
  echo "Processing $ID : $FILENAME"
  mkdir -p ${TRIM_DIR}/${ID}
  time TrimmomaticPE \
    -threads $N_THREADS \
    -phred33 \
    ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
    ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R1_trim_paired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R1_trim_unpaired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R2_trim_paired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R2_trim_unpaired.fastq.gz \
    -trimlog ${TRIM_DIR}/${ID}/${ID}_trim.log \
    ILLUMINACLIP:${ADAPTER}:2:30:10:2:True \
    LEADING:3 \
    TRAILING:3 \
    MINLEN:36 \
    > ${TRIM_DIR}/${ID}/${ID}_trim.runinfo 2>&1
done
```

```{bash}
ls -l $TRIM_DIR
```

# Post-trimming QC

At this point we could run FastQC and MultiQC on the trimmed FASTQs pairs to see if statistics have improved, but we will skip that for now.

```{bash eval=F}
for DIR in ${TRIM_DIR}/* ; do
  ID=$(basename ${DIR})
  echo "Processing ${ID}"
  mkdir ${FASTQC_TRIM_DIR}/${ID}
  fastqc \
    --threads ${N_THREADS} \
    --extract \
    --outdir ${FASTQC_TRIM_DIR}/${ID} \
    ${TRIM_DIR}/${ID}/${ID}_R1_trim_paired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R2_trim_paired.fastq.gz \
    > ${FASTQC_TRIM_DIR}/${ID}/${ID}.log 2>&1
done

multiqc ${FASTQC_TRIM_DIR} --outdir ${MULTIQC_TRIM_DIR}
```

```{bash}
cp ${MULTIQC_TRIM_DIR}/${ID}/multiqc_report.html ~/multiqc_report_trim.html
open ${RS_SERVER_URL}/files/multiqc_report_trim.html
```

