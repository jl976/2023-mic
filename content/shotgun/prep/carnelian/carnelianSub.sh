inputfastadir=/work/mrl17/carnelianhome/inputfastaSub
modeldir=/work/mrl17/carnelianhome/model_dir
sampleout=/work/mrl17/carnelianhome/sampleoutSub
fragpl=/work/mrl17/FragGeneScan

cd /work/mrl17/carnelianhome/carnelian/
carnelian annotate -k 8 -n 1 $inputfastadir $modeldir $sampleout $fragpl
