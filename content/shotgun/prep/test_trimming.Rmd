---
output:
  md_document:
    variant: markdown_github
    toc: true
  html_document:
    toc: true
    toc_float: true
---

# Goals

- Produce quality reports for multiple samples using FastQC
- Combine multiple FastQC reports using MultiQC
- Trim low-quality reads and adapters using Trimmomatic
- Perform post-trimming QC

# Setup

First we load R libraries,

```{r}
library(tidyverse)
library(here)
```

Then we load the paths and directories pre-defined in the `config.R`,

```{r}
"content/config.R" %>%
    here() %>%
    source()
```

We also need to define some shell variables for later use in the bash chunks, using R function `Sys.setenv`,

```{r}
# Input
Sys.setenv(
  RAW_FASTQ_DIR = shotgun_data_dir,
  N_THREADS = 39
)
# Output
Sys.setenv(
  OUT_DIR = shotgun_out_dir,
  TRIM_DIR = file.path(shotgun_out_dir, "trimmomatic_universal"),
  FASTQMCF_DIR = file.path(shotgun_out_dir, "fastqmcf_universal"),
  ADAPTER_MCF = "/work/xq24/mic2023/matson_shotgun/test/illumina_universal_pe_mcf.fa",
  ADAPTER_TM = "/work/xq24/mic2023/matson_shotgun/test/illumina_universal_pe_tm.fa"
)
```

Let's check the values of the path variables,

```{bash}
echo $RAW_FASTQ_DIR
echo $N_THREADS
echo $OUT_DIR
echo $TRIM_DIR
echo $FASTQMCF_DIR
echo $ADAPTER_MCF
echo $ADAPTER_TM
```
And make the output directories, the flag `-p` is to suppress the error if the directory's already existed and make parent directories as needed. 

```{bash}
mkdir -p $TRIM_DIR
mkdir -p $FASTQMCF_DIR
```

Let's check to make sure the directories are properly created. We will run `ls` and check that these directories now exist in the `$OUT_DIR` directory.

```{bash}
ls -l $OUT_DIR
```


# Adapters

## fastq-mcf
Universal adapter source: https://github.com/s-andrews/FastQC/blob/master/Configuration/adapter_list.txt

"Adapter sequences with _5p in their label will match 'end's, and sequences with _3p in their label will match 'start's, otherwise the 'end' is auto-determined." (fastq-mcf -h)

```{bash}
cat ${ADAPTER_MCF}
```

## trimmomatic 

"For 'Palindrome' clipping, a matched pair (or multiple matched pairs) of adapter sequences must be provided. The sequence names should both start with 'Prefix', and end in '/1' for the forward adapter and '/2' for the reverse adapter. The part of the name between 'Prefix' and '/1' or '/2' must match exactly within each pair. 

Those with names ending in '/1' or '/2' will be checked only against the forward or reverse read respectively. All other sequences will be checked against both the forward and reverse read."

```{bash}
cat ${ADAPTER_TM}
```

# Test data

## pseudo reads: 

- Adapters:
    - AGATCGGAAGAG (ADAPTER)
    - CTCTTCCGATCT (ADAPTER_RC)
- R1 (modified from @SRR6000890.20220, 151bp)
    - ATAACGATACGGAAGGATGGGAAATCCGCACGTACAAATATCTGGAGAATGTCGGTATAGAAGACCTGAGCTTCG (75bp)
    - ACGTATGACTAG (random 12bp)
    - CTATCTCTTTTAACTAAAAAAAGAGCGTAAACAATAAACGTGGCACGCTAGTTGTACGTGATAT (64bp)
- R2 (modified from @SRR6000890.20220, 150bp)
    - GTCTATTTGAAGAAGAGGATTCTGCAAAATTCTCATTCTCGTCATTCGCTTGATTCTGAGTATA (64bp)
    - TGTATTTTGTTT (random 12bp)
    - CTGGTGGGTGGAAGTTCGATTTTGATAGGGGGGACGTTTGCCGATCCCGAACAATACACGCAAAATGTTACCTA (74bp)

## fastq-mcf
```{bash}
# -P phred score type
# -S save all discarded reads
# -q quality threshold
# -l Minimum remaining sequence length (19)
DIR=/work/xq24/mic2023/matson_shotgun/test
fastq-mcf \
  -P 33 \
  -S \
  -q 20 \
  -l 1 \
  -o ${DIR}/fastqmcf_r1.fastq.gz \
  -o ${DIR}/fastqmcf_r2.fastq.gz \
  ${ADAPTER_MCF} \
  ${DIR}/test_r1.fastq.gz \
  ${DIR}/test_r2.fastq.gz \
  > ${DIR}/fastqmcf.runinfo 2>&1
```

```{bash}
DIR=/work/xq24/mic2023/matson_shotgun/test
echo "Paired R1:"
zcat ${DIR}/fastqmcf_r1.fastq.gz | head -n 12
echo ""; echo "Paired R2:"
zcat ${DIR}/fastqmcf_r2.fastq.gz | head -n 12
echo ""; echo "Unpaired R1:"
zcat ${DIR}/fastqmcf_r1.fastq.skip.gz | head -n 4
echo ""; echo "Unpaired R2:"
zcat ${DIR}/fastqmcf_r2.fastq.skip.gz | head -n 4
echo ""; echo "log:"
cat ${DIR}/fastqmcf.runinfo
```


## Trimmomatic
```{bash}
DIR=/work/xq24/mic2023/matson_shotgun/test
simpleClipThreshold=7
TrimmomaticPE \
  -threads $N_THREADS \
  -phred33 \
  ${DIR}/test_r1.fastq.gz \
  ${DIR}/test_r2.fastq.gz \
  ${DIR}/trimmomatic_paired_R1.fastq.gz \
  ${DIR}/trimmomatic_unpaired_R1.fastq.gz \
  ${DIR}/trimmomatic_paired_R2.fastq.gz \
  ${DIR}/trimmomatic_unpaired_R2.fastq.gz \
  -trimlog ${DIR}/trimmomatic.log \
  ILLUMINACLIP:${ADAPTER_TM}:2:30:${simpleClipThreshold}:2:True \
  LEADING:3 TRAILING:3 MINLEN:1 \
  > ${DIR}/trimmomatic.runinfo 2>&1
# default:
#MINLEN:36
#simpleClipThreshold=10
```

```{bash}
DIR=/work/xq24/mic2023/matson_shotgun/test
echo "Paired R1:"
zcat ${DIR}/trimmomatic_paired_R1.fastq.gz | head -n 4
echo ""; echo "Paired R2:"
zcat ${DIR}/trimmomatic_paired_R2.fastq.gz | head -n 4
echo ""; echo "Unpaired R1:"
zcat ${DIR}/trimmomatic_unpaired_R1.fastq.gz | head -n 4
echo ""; echo "Unpaired R2:"
zcat ${DIR}/trimmomatic_unpaired_R2.fastq.gz | head -n 4
echo ""; echo "log:"
cat ${DIR}/trimmomatic.log

# the read name
# the surviving sequence length
# the location of the first surviving base, aka. the amount trimmed from the start
# the location of the last surviving base in the original read
# the amount trimmed from the end
```


## cutadapt

```{bash}
# python3 -m pip install cutadapt
# cutadapt -h
DIR=/work/xq24/mic2023/matson_shotgun/test
~/.local/bin/cutadapt \
  --cores $N_THREADS \
  --minimum-length 1 \
  -a AGATCGGAAGAG \
  -A AGATCGGAAGAG \
  -o ${DIR}/cutadapt_paired_R1.fastq.gz \
  -p ${DIR}/cutadapt_paired_R2.fastq.gz \
  --info-file ${DIR}/cutadapt.log \
  ${DIR}/test_r1.fastq.gz \
  ${DIR}/test_r2.fastq.gz \
  > ${DIR}/cutadapt.runinfo 2>&1
```


# Matson shotgun data

## fastq-mcf
```{bash eval=F}
for FILENAME in ${RAW_FASTQ_DIR}/SRR600089[0-4]_1.fastq.gz ; do
  ID=$(basename $FILENAME _1.fastq.gz)
  echo "Processing $ID : $FILENAME"
  mkdir -p ${FASTQMCF_DIR}/${ID}
  fastq-mcf \
    -P 33 \
    -S \
    -q 20 \
    -x 36 \
    -o ${FASTQMCF_DIR}/${ID}/${ID}_R1_trim.fastq.gz \
    -o ${FASTQMCF_DIR}/${ID}/${ID}_R2_trim.fastq.gz \
    ${ADAPTER_MCF} \
    ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
    ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz \
    > ${FASTQMCF_DIR}/${ID}/${ID}_fastqmcf.runinfo 2>&1
done
```

```{bash}
ls -l ${FASTQMCF_DIR}
```

## Trimmomatic
```{bash}
simpleClipThreshold=7
#for FILENAME in ${RAW_FASTQ_DIR}/SRR600089[0-4]_1.fastq.gz ; do
#for FILENAME in ${RAW_FASTQ_DIR}/*_1.fastq.gz ; do
for FILENAME in ${RAW_FASTQ_DIR}/SRR6000{899,900,901,902,903,905,906,907,908,909,910,911,912,920,921,931,932,940,942,943,944,945,946,947,949}_1.fastq.gz ; do
  ID=$(basename $FILENAME _1.fastq.gz)
  echo "Processing $ID : $FILENAME"
  mkdir -p ${TRIM_DIR}/${ID}
  TrimmomaticPE \
    -threads $N_THREADS \
    -phred33 \
    ${RAW_FASTQ_DIR}/${ID}_1.fastq.gz \
    ${RAW_FASTQ_DIR}/${ID}_2.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R1_trim_paired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R1_trim_unpaired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R2_trim_paired.fastq.gz \
    ${TRIM_DIR}/${ID}/${ID}_R2_trim_unpaired.fastq.gz \
    -trimlog ${TRIM_DIR}/${ID}/${ID}_trim.log \
    ILLUMINACLIP:${ADAPTER_TM}:2:30:${simpleClipThreshold}:2:True \
    LEADING:3 \
    TRAILING:3 \
    MINLEN:36 \
    > ${TRIM_DIR}/${ID}/${ID}_trim.runinfo 2>&1
done
```

