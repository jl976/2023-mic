---
title: "Untitled"
output: html_document
date: "2023-02-03"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(fs)
library(dplyr)
kaiju_db_base="/hpc/group/mic-2023/kaijudb"
```


```{r eval=FALSE, include=FALSE}
db_subdir="refseq_virus"
kaiju_db_dir=file.path(kaiju_db_base, db_subdir)
dir_create(kaiju_db_dir, recurse = TRUE)

kaiju_virus_url = "https://kaiju.binf.ku.dk/database/kaiju_db_viruses_2022-03-29.tgz"

kaiju_virus_url %>%
  basename %>%
  file.path(kaiju_db_dir, .) ->
  kaiju_virus_tgz

download.file(kaiju_virus_url,kaiju_virus_tgz)
untar(kaiju_virus_tgz, exdir=kaiju_db_dir)
file_delete(kaiju_virus_tgz)
```

```{r}
downloadIndex <- function(index_url, db_subdir_name,db_parent_dir) {
  
  kaiju_db_dir=file.path(kaiju_db_base, db_subdir_name)
  dir_create(db_parent_dir, recurse = TRUE)
  
  
  
  index_url %>%
    basename %>%
    file.path(kaiju_db_dir, .) ->
    index_tgz_path
  
  download.file(index_url,index_tgz_path)
  untar(index_tgz_path, exdir=kaiju_db_dir)
  file_delete(index_tgz_path)
}

```

```{r}
downloadIndex(index_url = "https://kaiju.binf.ku.dk/database/kaiju_db_viruses_2022-03-29.tgz",
              db_subdir_name="refseq_virus",
              db_parent_dir=kaiju_db_base)
```





