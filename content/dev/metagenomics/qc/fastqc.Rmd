---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Bash
    language: bash
    name: bash
---

<!-- #region -->
# Quality Control
The notebook will go through a basic quality analysis of the raw FASTQs using a program named FastQC to analyzes the FASTQ files and another program named MultiQC to summarize the results across multiple FASTQs


## Shell Variables
First we need to define some variables!
<!-- #endregion -->

```{bash}
set -u

# Directory for input FASTQs
RAW_FASTQ_DIR="/data/sra_data"

# Directory for output from FastQC and MultiQC
OUT_DIR=$HOME/scratch/bioinf_intro
QC=$OUT_DIR/qc_output

TOTAL_THREADS=20
```

## Making New Directories

```{bash}
mkdir -p $QC
```

Now let's check to be sure that worked.  We will run `ls` and check that these directories now exist in the `$OUT_DIR` directory.

```{bash}
ls $OUT_DIR
```

## FastQC
Let's run some basic analysis using a program called fastqc.  This shouldn't take too long.  Remember that while "long" jobs are running it will say "In [\*]:" in the left margin, once it is done, a number will replace the asterisk.

If you need to know how to run a program, most unix programs will give you some information about themselves if you run with a `-h` or `--help` command line flag

```{bash}
fastqc -h
```

Let's pick a file to run on. Since we don't want to wait too long for it to run, let's pick the smallest file for now.

```{bash}
ls -lSrh $RAW_FASTQ_DIR
```

<!-- #region -->
- `--threads $TOTAL_THREADS` tells FastQC to run using `$TOTAL_THREADS` CPU cores, which will speed things.
- `--outdir $QC` tells FastQC to output the results to the directory refered to in the `$QC` variable. 


If you want to know what `--extract` does, check the help information above.
<!-- #endregion -->

```{bash}
fastqc --threads $TOTAL_THREADS --extract --outdir $QC $RAW_FASTQ_DIR/SRR12804466_1.fastq.gz
```

Once fastqc is done running we can view the results by finding the output in the Jupyter browser, it should be in:

```{bash}
echo $QC
```

## MultiQC
FastQC is a useful tool, but it has one problem: it generates one report for each FASTQ file.  When you have more than a handful of FASTQs (as most projects will), it is tedious to look at each one, and there is no simple way to compare them.

MultiQC is a solution to this problem.  It mines the results from FastQC (and other HTS analysis tools) and generates a report that summarizes results for all the FASTQs analyzed.

### Run FastQC and MultiQC on several FASTQs
Let's run FASTQs on a few more FASTQs to see how MultiQC can help us look at results from multiple FASTQ files.

```{bash}
fastqc --quiet --threads $TOTAL_THREADS --extract --outdir $QC $RAW_FASTQ_DIR/SRR1280446[457]_1.fastq.gz -o $QC
```

```{bash}
multiqc -h
```

```{bash}
multiqc $QC --outdir $QC
```

Once multiqc is done running we can view the results by finding the output in the Jupyter browser, it should be in a file named `multiqc_report.html` in :

```{bash}
echo $QC
```
