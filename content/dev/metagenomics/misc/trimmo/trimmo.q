#!/bin/bash
#SBATCH --job-name=trimmomatic
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=16G
#SBATCH --output=slurm-trimmo-%J.stdout
#SBATCH --error=slurm-trimmo-%J.stderr

trimmo () {
    local stem=${1}
    local R1=${2}
    local R2=${3}

    mkdir -p ${procdir}/${stem}/

    ${mysingularity} exec \
	--bind ${fqdir}:/mnt/fqdir/:ro \
	--bind ${procdir}:/mnt/procdir:rw \
	--bind ${trimmofadir}:/mnt/trimmofadir/:ro \
	--containall \
	${mysif} \
	java -jar ${trimmojar} PE \
	-phred33 \
	-threads ${SLURM_JOB_CPUS_PER_NODE} \
	/mnt/fqdir/${R1} /mnt/fqdir/${R2} \
	/mnt/procdir/${stem}/${stem}-R1-trim-paired.fastq.gz \
	/mnt/procdir/${stem}/${stem}-R1-trim-unpaired.fastq.gz \
	/mnt/procdir/${stem}/${stem}-R2-trim-paired.fastq.gz \
	/mnt/procdir/${stem}/${stem}-R2-trim-unpaired.fastq.gz \
	-trimlog /mnt/procdir/${stem}/${stem}-trim.log \
	ILLUMINACLIP:/mnt/trimmofadir/${trimmofa}:2:30:10:2:True \
	LEADING:3 TRAILING:3 MINLEN:36
}



env
set -x

echo "Number of CPUs per node"
echo ${SLURM_CPUS_ON_NODE}
echo "Amount of memory per node"
echo ${SLURM_MEM_PER_NODE}


mysingularity=apptainer
mysif=/work/owzar001/singularity/dcibioinformaticsHTS-v0.2.sif 
trimmojar=/scif/apps/trimmomatic/Trimmomatic-0.39/trimmomatic-0.39.jar
trimmofadir=/work/owzar001/MIC2023/
trimmofa=universal.fa


fqdir=/hpc/group/mic-2023/rawdata/matson_shotgun/
procdir=/work/owzar001/MIC2023/

trimmo SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz 

