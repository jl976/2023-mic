#!/bin/bash

#SBATCH --job-name=gatk-index
#SBATCH --mail-user=owzar0041@duke.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=256G
#SBATCH --output=slurm-gatk-workflow-%J.stdout
#SBATCH --error=slurm-gatk-workflow-%J.stderr

env
set -x

echo "Number of CPUs per node"
echo ${SLURM_CPUS_ON_NODE}
echo "Amount of memory per node"
echo ${SLURM_MEM_PER_NODE}


singularity=apptainer
${singularity} --version

mysif=/work/owzar001/singularity/dcibioinformaticsHTS-v0.2.sif 
picardjar=/scif/apps/picardtools/picard.jar
heapsize=32g
bwacores=${SLURM_JOB_CPUS_PER_NODE}

source gatk-workflow.sh
md5sum gatk-workflow.sh

fadir=/hpc/group/mic-2023/references/GCA/
fafile=GCA_000001405.15_GRCh38_no_alt_analysis_set.fna

fqdir=/hpc/group/mic-2023/rawdata/matson_shotgun/

tmpdir=/work/owzar001/tmpdir/
procdir=/work/owzar001/MIC2023/bwa/



#time gatkindex ${fadir} ${fafile}


mystem=SRR6000893
time bwastep ${fqdir} ${mystem}_1.fastq.gz  ${mystem}_2.fastq.gz  \
     ${mystem}-ID  ${mystem}-LB  ${mystem}-SM  ${mystem}-PU
time dedupstep  ${mystem} "${mystem}-ID-RG-coordsorted.bam"
time getflagstat ${mystem}-dedup.bam

mystem=SRR6000869

time bwastep ${fqdir} ${mystem}_1.fastq.gz  ${mystem}_2.fastq.gz  \
     ${mystem}-ID  ${mystem}-LB  ${mystem}-SM  ${mystem}-PU
time dedupstep  ${mystem} "${mystem}-ID-RG-coordsorted.bam"
time getflagstat ${mystem}-dedup.bam
