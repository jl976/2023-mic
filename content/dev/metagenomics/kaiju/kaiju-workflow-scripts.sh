#!/bin/bash

kaijuworkflow() {
    local fqdir=$1
    local fqstem=$2
    local R1=$3
    local R2=$4
    local rank=$5
    local procdir=${procdir}/${fqstem}/

    local kaiout=/mnt/procdir/${fqstem}-${kaijufmi}-kaiju.out
    local kaism=/mnt/procdir/${fqstem}-kaiju-${kaijufmi}-${rank}-summary.tsv
    local kainm=/mnt/procdir/${fqstem}-kaiju-${kaijufmi}-names.out
    local kaikrona=${kaiout}.krona
    local kaihtml=${kaiout}.html

    echo ${kaikrona}
    echo ${kaihtml}
    
    mkdir -p ${procdir}

    apptainer exec \
      --containall \
      --bind ${kaijudbdir}:/mnt/kaijudb/:ro \
      --bind ${fqdir}:/mnt/fqdir/:ro \
      --bind ${procdir}:/mnt/procdir/:rw \
      ${mysif} ${kaijupgmdir}bin/kaiju \
      -v \
      -z ${SLURM_CPUS_PER_TASK} \
      -t /mnt/kaijudb/nodes.dmp \
      -f /mnt/kaijudb/${kaijufmi} \
      -i /mnt/fqdir/${R1} -j /mnt/fqdir/${R2} \
      -o ${kaiout}

    apptainer exec \
      --containall \
      --bind ${kaijudbdir}:/mnt/kaijudb/:ro \
      --bind ${procdir}:/mnt/procdir/:rw \
      ${mysif} ${kaijupgmdir}/bin/kaiju2krona  \
      -t /mnt/kaijudb/nodes.dmp \
      -n /mnt/kaijudb/names.dmp \
      -v \
      -i ${kaiout} \
      -o ${kaikrona}
   
#    apptainer exec \
#      --containall \
#      --bind ${procdir}:/mnt/procdir/:rw \
#      ${mysif} ktImportText  \
#      -v \
#      -o ${kaihtml} \
#      ${kaikrona}

    apptainer exec \
      --containall \
      --bind ${procdir}:/mnt/procdir/:rw \
      --bind ${kaijudbdir}:/mnt/kaijudb/:ro \
      ${mysif} ${kaijupgmdir}/bin/kaiju2table \
      -t /mnt/kaijudb/nodes.dmp \
      -n /mnt/kaijudb/names.dmp \
      -r ${rank} \
      -o ${kaism} \
      ${kaiout}

    apptainer exec \
      --containall \
      --bind ${kaijudbdir}:/mnt/kaijudb/:ro \
      --bind ${procdir}:/mnt/procdir/:rw \
      ${mysif} ${kaijupgmdir}/bin/kaiju-addTaxonNames \
      -t /mnt/kaijudb/nodes.dmp \
      -n /mnt/kaijudb/names.dmp \
      -i ${kaiout} \
      -o ${kainm}
}
