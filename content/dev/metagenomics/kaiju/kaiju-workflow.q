#!/bin/bash

#SBATCH --job-name=kaiju-workflow
#SBATCH --mail-type=END,FAIL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=26
#SBATCH --mem=256G
#SBATCH --output=slurm-kaiju-workflow-%J.stdout
#SBATCH --error=slurm-kaiju-workflow-%J.stderr

set -x

echo "Number of CPUs per node"
echo ${SLURM_CPUS_ON_NODE}
echo "Amount of memory per node"
echo ${SLURM_MEM_PER_NODE}

source kaiju-workflow-scripts.sh
md5sum kaiju-workflow-scripts.sh


mysif=/work/owzar001/MIC2023/kaiju.sif

# kaiju program root directory and database
kaijupgmdir=/opt/kaiju/kaiju-1.9.2/

# Root directory for fastq files
fqdir=/hpc/group/mic-2023/rawdata/matson_shotgun/
# Output directory
procdir=/work/${USER}/mic2023/

#--------------
# RefSeq Virus
#--------------
kaijudbdir=/hpc/group/mic-2023/kaijudb/refseq_virus/
kaijufmi=kaiju_db_viruses.fmi
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz species
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz species


#--------------
# RefSeq
#--------------
kaijudbdir=/hpc/group/mic-2023/kaijudb/refseq/
kaijufmi=kaiju_db_refseq.fmi
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz species
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz species

#--------------
# NR
#--------------
kaijudbdir=/hpc/group/mic-2023/kaijudb/nr/
kaijufmi=kaiju_db_nr_euk.fmi
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz genus
time kaijuworkflow ${fqdir} SRR6000893 SRR6000893_1.fastq.gz SRR6000893_2.fastq.gz species
time kaijuworkflow ${fqdir} SRR6000869 SRR6000869_1.fastq.gz SRR6000869_2.fastq.gz species
