---
title: DADA2 From FASTQs to OTUs
output: html_document
editor_options: 
  chunk_output_type: console
---

Here we walk through version 1.6 of the DADA2 pipeline on a small multi-sample dataset. Our starting point is a set of Illumina-sequenced paired-end fastq files that have been split (or "demultiplexed") by sample and from which the barcodes/adapters have already been removed. The end product is an **amplicon sequence variant (ASV) table**, a higher-resolution analogue of the traditional "OTU table", which records the number of times each amplicon sequence variant was observed in each sample. We also assign taxonomy to the output sequences, and demonstrate how the data can be imported into the popular [phyloseq](https://joey711.github.io/phyloseq/) R package for the analysis of microbiome data.

-----------------------

# Starting point

This workflow assumes that your sequencing data meets certain criteria:

* Samples have been demultiplexed, i.e. split into individual per-sample fastq files.
* Non-biological nucleotides have been removed, e.g. primers, adapters, linkers, etc.
* If paired-end sequencing data, the forward and reverse fastq files contain reads in matched order.

If these criteria are not true for your data (**are you sure there aren't any primers hanging around?**) you need to remedy those issues before beginning this workflow. See DADA2 FAQ for some recommendations for common issues.

# Getting ready
## Load Libraries

First we load libraries.

```{r, libraries}
library(dada2)
library(readr)
library(stringr)
library(dplyr)
library(tibble)
library(magrittr)
library(phyloseq)
library(ggplot2)
library(fs)
library(tidyr)
library(here)

```

```{r}
"content/config.R" %>%
    here() %>%
    source()

```

We will work with are the Matson amplicon data. We have downloaded this data from SRA which requires that data have been demultiplexed already. We also subsampled this dataset to 3% of its original number of records for the purpose of demonstrating the dada2 pipeline quickly.


```{r, files_and_directories}
list.files(amplicon_subsample_dir) # matson data

```

If the package successfully loaded and your listed files match those here, you are ready to go through the DADA2 pipeline.

## Set Up Paths
We need to set up a "scratch" directory for saving files that we generate while running dada2, but don't need to save long term.

```{r, other_paths}
# make directory for output
if (dir_exists(dada_out_dir)) {
  dir_delete(dada_out_dir)
}
dir_create(dada_out_dir)

dada_out_dir # this is the name of our scratch directory

```

&nbsp;

# Filter and Trim

First we read in the names of the fastq files, and perform some string manipulation to get lists of the forward and reverse fastq files in matched order.

## List of forward FASTQs

```{r, forward_fastq_paths}
# Forward and reverse fastq filenames have format: 
# SAMPLENAME.forward.fastq.gz
# SAMPLENAME.reverse.fastq.gz
amplicon_subsample_dir %>%
    list.files(pattern="1.fastq", 
               full.names=TRUE) %>%
    sort ->
    fnFs

```

### Reality Check on Files

```{r}
print(fnFs)

```


## List of forward FASTQs without "unmatched"

Note that this dataset was downloaded from SRA so we do not have any "unmatched" reads from the demultiplexing step. If you were to run dada2 after demultiplexing your own data, then these "unmatched" reads would also be in this directory and need to be excluded. Here is an example of how you could do that. 

```{r}
amplicon_subsample_dir %>%
    list.files(pattern="1.fastq",
               full.names=TRUE) %>%
    str_subset(pattern="unmatched", negate=TRUE) %>% # this should get rid of the "unmatched" file
    sort ->
    fnFs
print(fnFs) 

```

Let's do the same thing for the "reverse" read FASTQs

## List of reverse FASTQs without "unmatched"

```{r, reverse_fastq_paths}
amplicon_subsample_dir %>%
    list.files(pattern="2.fastq",
               full.names=TRUE) %>%
    str_subset(pattern="unmatched", negate=TRUE) %>% # this should get rid of the "unmatched" file
    sort ->
    fnRs
print(fnRs)

```

## List of Sample Names

```{r, filenames}
# Extract sample names, assuming filenames have format: SAMPLENAME.X.fastq, where X is reverse or forward

forward_fastq_suffix = ".1.fastq.gz"

fnFs %>% 
    basename %>%
    str_remove(forward_fastq_suffix) ->
    sample.names

print(sample.names)

```

### Double Check Sample Names
and just to be sure, let's check that we get the same result when we generate samples names from the list of reverse FASTQs

```{r}
reverse_fastq_suffix = ".2.fastq.gz"

fnRs %>% 
    basename %>%
    str_remove(reverse_fastq_suffix) ->
    rev.sample.names

identical(sample.names,rev.sample.names)

```

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** The string manipulations may have to be modified if using a different filename format.</div>

&nbsp;

## Examine quality profiles of forward and reverse reads

We start by visualizing the quality profiles of the forward reads:

```{r}
list.files(amplicon_subsample_dir)

```

```{r, see-quality-F}
plotQualityProfile(fnFs[1])

```

The first sample has 722 reads.

- This is a grey-scale heatmap; dark regions correspond to higher frequency
- Green line = mean
- Orange line = median
- Dashed orange lines are the 25th and 75th quantiles


Let's look at more samples

```{r, see-quality-F-all}
plotQualityProfile(fnFs[1:10], aggregate = T) # warning: this will take longer if you ask for more samples

```

The forward reads are good quality (i.e. the median quality is typically > 30 over all sequencing cycles). We generally advise trimming the last few nucleotides to avoid less well-controlled errors that can arise there. These quality profiles do not suggest that any additional trimming is needed, so we will truncate the forward reads at position 145 (trimming the last 5 nucleotides).  

Now we visualize the quality profile of the reverse reads:

```{r, see-quality-R}
plotQualityProfile(fnRs[1:2])

```

Again, let's look at more samples

```{r, see-quality-R-all}
plotQualityProfile(fnRs[1:10], aggregate = T)

```

The reverse reads are of significantly worse quality, and it drops off a little at the end, which is common in Illumina sequencing. This isn't too worrisome, as DADA2 incorporates quality information into its error model which makes the algorithm [robust to lower quality sequences](https://twitter.com/bejcal/status/771010634074820608), but trimming as the average qualities drop off will improve the algorithm's sensitivity to rare sequence variants. Based on these profiles, we will truncate the reverse reads at position 140 where the quality distribution dips down.  As with the forward reads, the first ~10bp are somewhat lower quality so we will trim 10bp from the left also.

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** **Your reads must still overlap after truncation in order to merge them later!** The tutorial is using 150bp PE V4 sequence data, so the forward and reverse reads overlap by about 50bp.  When using data with limited overlap `truncLen` must be large enough to maintain `20 + biological.length.variation` nucleotides of overlap between them.  When using data that overlap more (e.g. 250 PE V4) trimming can be completely guided by the quality scores. 

Non-overlapping primer sets are supported as well with `mergePairs(..., justConcatenate=TRUE)` when performing merging.</div>

&nbsp;

## Perform filtering and trimming

Assign the filenames for the filtered fastq.gz files.

```{r, filt-names}
filt_path <- file.path(dada_out_dir, "filtered") # Place filtered files in filtered/ subdirectory
filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt.fastq.gz"))
filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt.fastq.gz"))

```

We'll use standard filtering parameters: `maxN=0` (DADA2 requires no Ns), `truncQ=2`, `rm.phix=TRUE` and `maxEE=2`. The `maxEE` parameter sets the maximum number of "expected errors" allowed in a read, which is [a better filter than simply averaging quality scores](http://www.drive5.com/usearch/manual/expected_errors.html).

### Filter the forward and reverse reads
Filtering (e.g. removing a read because it has overall bad quality) must be done in such a way that forward and reverse reads are kept in sync: if a reverse read is filtered out because of low quality, its partner forward read must *also* be removed, even if it passes.  `filterAndTrim` does this if you pass it the forward and reverse FASTQs.

The first ~10bp of R1 and R2 are somewhat lower quality, which is very common for Illumina data.  Let's trim this with `trimLeft=10` (note: this will result in shorter amplicons, where trimming on right end of a read should not change amplicon length after it is filtered).

```{r, filter}
filt.out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, trimLeft=10, truncLen=c(145,140),
              maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=FALSE) # On Windows set multithread=FALSE
# finished in ~ 1 min (longest step after assignTaxonomy)

```

Make sure to follow-up on any warning messages:

The output of `filterAndTrim` has a table summary so we can quickly check the number of reads in and out for each sample like this.
```{r}
filt.out %>%
  data.frame()%>%
  arrange(reads.out) %>%
  head(n = 10)

```

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** The standard filtering parameters are starting points, not set in stone. For example, if too few reads are passing the filter, considering relaxing `maxEE`, perhaps especially on the reverse reads (eg. `maxEE=c(2,5)`). If you want to speed up downstream computation, consider tightening `maxEE`. For paired-end reads consider the length of your amplicon when choosing `truncLen` as your reads must overlap after truncation in order to merge them later.</div>

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;margin-top: 15px;">**<span style="color:red">If using this workflow on your own data:</span>** For common ITS amplicon strategies, it is undesirable to truncate reads to a fixed length due to the large amount of length variation at that locus. That is OK, just leave out `truncLen`. Make sure you removed the forward and reverse primers from both the forward and reverse reads though!</div>

&nbsp;

# Learn the Error Rates

The DADA2 algorithm depends on a parametric error model (`err`) and every amplicon dataset has a different set of error rates. The `learnErrors` method learns the error model from the data, by alternating estimation of the error rates and inference of sample composition until they converge on a jointly consistent solution. As in many optimization problems, the algorithm must begin with an initial guess, for which the maximum possible error rates in this data are used (the error rates if only the most abundant sequence is correct and all the rest are errors).

```{r, error=TRUE}
errF <- learnErrors(filtFs, multithread=TRUE) 

```

An important note: If some samples ended up with zero reads after `filterAndTrim`, the resulting fastq will not exist because the function didn't create empty filtered FASTQs. This can cause errors downstream if you try to feed `learnErrors` the name of a file that does not exist.  Here is a quick way to check that we have all the files we expect:

```{r}
file_exists(filtFs) %>%
    as.data.frame

```

If filenames do not exist as files, those were probably ones with zero reads after filtering.  Here is how we can clean up filtFs and filtRs to remove missing files. (**Not applicable in this case**)

```{r}
filtFs = filtFs[file_exists(filtFs)]
filtRs = filtRs[file_exists(filtRs)]

```

Now let's try it again


```{r, error=TRUE}
errF <- learnErrors(filtFs, multithread=TRUE) 
# finished in < 1 min

errR <- learnErrors(filtRs, multithread=TRUE)
# finished in < 1 min

```

It is always worthwhile, as a sanity check if nothing else, to visualize the estimated error rates:

```{r, plot-errors}
plotErrors(errF, nominalQ=TRUE)

```

The error rates for each possible transition (eg. A->C, A->G, ...) are shown. In general, you expect the frequency of sequencing error to decrease where you have higher quality scores.

Points are the observed error rates for each consensus quality score. The red line shows the error rates expected under the nominal definition of the Q-value **Is the Q-value introduced in the lecture?**. The black line shows the estimated error rates after convergence of the model we just fit using learnErrors().  Here the black line (the estimated rates) fits the observed rates well, and the error rates drop with increased quality as expected. Everything looks reasonable and we proceed with confidence.

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** Parameter learning is computationally intensive, so by default the `learnErrors` function uses only a subset of the data (the first 1M reads). If the plotted error model does not look like a good fit, try increasing the `nreads` parameter to see if the fit improves.</div>

&nbsp;

# Dereplication

Dereplication combines all identical sequencing reads into into "unique sequences" with a corresponding "abundance": the number of reads with that unique sequence. Dereplication substantially reduces computation time by eliminating redundant comparisons.

Dereplication in the DADA2 pipeline has one crucial addition from other pipelines: **DADA2 retains a summary of the quality information associated with each unique sequence**. The consensus quality profile of a unique sequence is the average of the positional qualities from the dereplicated reads. These quality profiles inform the error model of the subsequent denoising step, significantly increasing DADA2's accuracy.

**Dereplicate the filtered fastq files**

```{r, dereplicate, error=TRUE}
derepFs <- derepFastq(filtFs, verbose=TRUE)
# finished in < 1 min

derepRs <- derepFastq(filtRs, verbose=TRUE)
# finished in < 1 min

# Name the derep-class objects by the sample names
names(derepFs) <- sample.names
names(derepRs) <- sample.names

```

An important note: Notice that we generated our "sample.names" from the original list of FASTQs but it is possible for a sample to drop out during filtering and we do not want to generate a filename using a sample name that has been dropped.  We can re-evaluate the sample list based on the list of filtered FASTQs.

```{r}
filtFs %>% 
  basename %>%
  str_replace("_F_filt.fastq.gz","") ->
  sample.names

```

Now let's try the dereplication again

```{r, dereplicate_tryagain}
derepFs <- derepFastq(filtFs, verbose=TRUE)
derepRs <- derepFastq(filtRs, verbose=TRUE)
# Name the derep-class objects by the sample names
names(derepFs) <- sample.names
names(derepRs) <- sample.names

```

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** The tutorial dataset is small enough to easily load into memory. If your dataset exceeds available RAM, it is preferable to process samples one-by-one in a streaming fashion: see the [DADA2 Workflow on Big Data](bigdata.html) for an example.</div>

&nbsp;

# Sample Inference

We are now ready to apply the core sequence-variant inference algorithm to the dereplicated data. 

**Infer the sequence variants in each sample**

```{r, dada}
dadaFs <- dada(derepFs, err=errF, multithread=TRUE)
# finished in < 1 min

dadaRs <- dada(derepRs, err=errR, multithread=TRUE)
# finished in < 1 min

```

Inspecting the dada-class object returned by dada:

```{r, see-dada}
dadaFs[[1]]

```

The DADA2 algorithm inferred `r length(dadaFs[[1]]$sequence)` real sequence variants from the `r length(dadaFs[[1]]$map)` unique sequences in the first sample. There is much more to the `dada-class` return object than this (see `help("dada-class")` for some info), including multiple diagnostics about the quality of each inferred sequence variant, but that is beyond the scope of an introductory tutorial.

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** All samples are simultaneously loaded into memory in the tutorial. If you are dealing with datasets that approach or exceed available RAM, it is preferable to process samples one-by-one in a streaming fashion: see the **[DADA2 Workflow on Big Data](bigdata.html)** for an example.</div>

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;margin-top: 15px;">**<span style="color:red">If using this workflow on your own data:</span>** By default, the `dada` function processes each sample independently, but pooled processing is available with `pool=TRUE` and that may give better results for low sampling depths at the cost of increased computation time. See our [discussion about pooling samples for sample inference](pool.html).</div>

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;margin-top: 15px;">**<span style="color:red">If using this workflow on your own data:</span>** DADA2 also supports 454 and Ion Torrent data, but [we recommend some minor parameter changes](faq.html#can-i-use-dada2-with-my-454-or-ion-torrent-data) for those sequencing technologies. The adventurous can explore `?setDadaOpt` for other adjustable algorithm parameters.</div>

&nbsp;

# Merge paired reads

Spurious sequence variants are further reduced by merging overlapping reads. The core function here is `mergePairs`, which depends on the forward and reverse reads being in matching order at the time they were dereplicated.

**Merge the denoised forward and reverse reads**:

```{r, merge}
mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, verbose=TRUE)
# finished in < 1 min
# Inspect the merger data.frame from one sample

```

We now have a `data.frame` for each sample with the merged `$sequence`, its `$abundance`, and the indices of the merged `$forward` and `$reverse` denoised sequences. Paired reads that did not exactly overlap were removed by `mergePairs`.

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** Most of your **reads** should successfully merge. If that is not the case upstream parameters may need to be revisited: Did you trim away the overlap between your reads?</div>

&nbsp;

# Construct sequence table

We can now construct a sequence table of our mouse samples, a higher-resolution version of the OTU table produced by traditional methods.

```{r, seqtab}
seqtab <- makeSequenceTable(mergers)
dim(seqtab)
# Inspect distribution of sequence lengths
table(nchar(getSequences(seqtab)))

```

The sequence table is a `matrix` with rows corresponding to (and named by) the samples, and columns corresponding to (and named by) the sequence variants. The lengths of our merged sequences all fall within the expected range for this V4 amplicon.

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** Sequences that are much longer or shorter than expected may be the result of non-specific priming, and may be worth removing (eg. `seqtab2 <- seqtab[,nchar(colnames(seqtab)) %in% seq(250,256)]`). This is analogous to "cutting a band" in-silico to get amplicons of the targeted length.</div>

&nbsp;

# Remove chimeras

The core `dada` method removes substitution and indel errors, but chimeras remain. Fortunately, the accuracy of the sequences after denoising makes identifying chimeras simpler than it is when dealing with fuzzy OTUs: all sequences which can be exactly reconstructed as a bimera (two-parent chimera) from more abundant sequences.

**Remove chimeric sequences**:

```{r, chimeras}
seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
dim(seqtab.nochim)
sum(seqtab.nochim)/sum(seqtab)

```

The fraction of chimeras varies based on factors including experimental procedures and sample complexity, but can be substantial. Here chimeras make up about `r round(100*(ncol(seqtab)-ncol(seqtab.nochim))/ncol(seqtab))`\% of the inferred sequence variants, but those variants account for only about `r round(100*(sum(seqtab)-sum(seqtab.nochim))/sum(seqtab))`\% of the total sequence reads. 

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** Most of your **reads** should remain after chimera removal (it is not uncommon for a majority of **sequence variants** to be removed though). If most of your reads were removed as chimeric, upstream processing may need to be revisited. In almost all cases this is caused by primer sequences with ambiguous nucleotides that were not removed prior to beginning the DADA2 pipeline.</div>

&nbsp;

# Track reads through the pipeline

As a final check of our progress, we'll look at the number of reads that made it through each step in the pipeline:

```{r, track}
getN <- function(x) sum(getUniques(x))
filt.out %>%
  as_tibble(rownames = "filename") %>%
  mutate(sample=str_replace(filename, forward_fastq_suffix,"")) %>%
  select(sample, input=reads.in, filtered=reads.out) ->
  track

sapply(dadaFs, getN) %>%
  enframe(name="sample", value="denoised") ->
  denoised
track %<>% full_join(denoised, by=c("sample"))

sapply(mergers, getN) %>%
  enframe(name="sample", value="merged") ->
  merged
track %<>% full_join(merged, by=c("sample"))

rowSums(seqtab) %>%
  enframe(name="sample", value="tabled") ->
  tabled
track %<>% full_join(tabled, by=c("sample"))

rowSums(seqtab.nochim) %>%
  enframe(name="sample", value="nonchim") ->
  nonchim
track %<>% full_join(nonchim, by=c("sample"))

track

```

## Plot Counts through pipeline

```{r}
track %>%
    gather(key="stage", value="counts", -c("sample")) %>%
    replace_na(list(counts = 0)) %>%
    mutate(stage=factor(stage, levels = c('input','filtered','denoised','merged','tabled','nonchim'))) %>%
    ggplot(mapping=aes(x=stage, y=counts, by=sample, group = sample)) +
    geom_line(alpha=0.5) +
        theme_classic()

```

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** This is a great place to do a last **sanity check**. Outside of filtering (depending on how stringent you want to be) there should no step in which a majority of reads are lost. If a majority of reads failed to merge, you may need to revisit the `truncLen` parameter used in the filtering step and make sure that the truncated reads span your amplicon. If a majority of reads failed to pass the chimera check, you may need to revisit the removal of primers, as the ambiguous nucleotides in unremoved primers interfere with chimera identification.</div>

&nbsp;

# Assign taxonomy

It is common at this point, especially in 16S/18S/ITS amplicon sequencing, to classify sequence variants taxonomically. The DADA2 package provides a native implementation of [the RDP's naive Bayesian classifier](http://www.ncbi.nlm.nih.gov/pubmed/17586664) for this purpose. The `assignTaxonomy` function takes a set of sequences and a training set of taxonomically classified sequences, and outputs the taxonomic assignments with at least `minBoot` bootstrap confidence. 

The DADA2 developers maintain [formatted training fastas for the RDP training set, GreenGenes clustered at 97\% identity, and the Silva reference database](training.html). For fungal taxonomy, the General Fasta release files from the [UNITE ITS database](https://unite.ut.ee/repository.php) can be used as is. 

We have downloaded a shared IBIEM copy of of the DADA2 formated Silva database and assigned its path to the `silva_ref` variable in the [Set Up Paths] section above, so you don't need to download it!

```{r, taxify}
taxa <- assignTaxonomy(seqtab.nochim, silva_ref, multithread=TRUE)
# This caused R to crash using the default memory settings of 2 GB!
# After increasing to 4 GB of mem, completed in 1-2 min (longest step)

```

**Optional:** The dada2 package also implements a method to make [species level assignments based on **exact matching**](assign.html#species-assignment) between ASVs and sequenced reference strains. Currently species-assignment training fastas are available for the Silva and RDP 16S databases. To follow the optional species addition step, download the `silva_species_assignment_v132.fa.gz` file, and place it in the directory with the fastq files.

```{r, species}
#taxa <- addSpecies(taxa, silva_species_ref)
# finished in ~ 1 min

```

Let's inspect the taxonomic assignments:

```{r, see-tax}
taxa.print <- taxa # Removing sequence rownames for display only
rownames(taxa.print) <- NULL
head(taxa.print)

```

<div style="border: 1px solid red;padding: 5px;background-color: #fff6f6;">**<span style="color:red">If using this workflow on your own data:</span>** If your reads do not seem to be appropriately assigned, for example lots of your bacterial 16S sequences are being assigned as `Eukaryota NA NA NA NA NA`, your reads may be in the opposite orientation as the reference database. Tell dada2 to try the reverse-complement orientation with `assignTaxonomy(..., tryRC=TRUE)` and see if this fixes the assignments.</div>

&nbsp;

---------------------------------------------------------

# Bonus: Handoff to phyloseq

The [phyloseq R package is a powerful framework for further analysis of microbiome data](https://joey711.github.io/phyloseq/). We now demonstrate how to straightforwardly import the tables produced by the DADA2 pipeline into phyloseq. We'll also add the sample metadata.

## Make Phyloseq Object
We can construct a simple sample data.frame based on the filenames. Usually this step would instead involve reading the sample data in from a file.

```{r, load_map}
md <- read_csv(matson_sample_csv) 
length(unique(md$SampleID)) # 81 samples in total
length(unique(track$sample)) # there were only 42 samples amplicon data

md %>%
  filter(SampleID %in% track$sample) %>%
  column_to_rownames("SampleID") %>%
  as.data.frame() -> meta.df
nrow(meta.df) # 42 rows

```

We can now construct a phyloseq object directly from the dada2 outputs.

```{r, make-phyloseq}
# Taxonomy table -- each row corresponds to an ASV in our count matrix
# create a shortname for each unique ASV so we don't have to use the unique ASV sequence
asv.id <- paste0("ASV_", seq(1:nrow(taxa)))
asv.seq <- row.names(taxa)
taxa.reorg <- cbind(taxa, asv.id, asv.seq)
colnames(taxa.reorg)
row.names(taxa.reorg) <- asv.id
tax <- tax_table(taxa.reorg)

# Count matrix -- these are really ASVs but in phyloseq-terminology we are going to store these in an "otu_table"
rownames(seqtab.nochim) # samples are rows
colnames(seqtab.nochim) # columns are asv.seqs
identical(colnames(seqtab.nochim), asv.seq)
colnames(seqtab.nochim) <- asv.id
otus <- otu_table(seqtab.nochim, taxa_are_rows=FALSE)

sd <- sample_data(meta.df)
ps <- phyloseq(otus,
               sd,
               tax)

ps
```

## Save Phyloseq as RDS

Any R object can be saved to an RDS file.  It is a good idea to do this for any object that is time consuming to generate and is reasonably small in size.  Even when the object was generated reproducibly, it can be frustrating to wait minutes or hours to regenerate when you are ready to perform downstream analyses.

We will do this for out phyloseq object to a file since it is quite small (especially compared to the size of the input FASTQ files), and there were several time consuming computational steps required to generate it.  

```{r}
amplicon_ps_rds
write_rds(ps, amplicon_ps_rds)
```

We can now confirm that it worked!

```{r}
loaded.ps = read_rds(amplicon_ps_rds)
print(loaded.ps)
```

We are now ready to use phyloseq!

## Visualize alpha-diversity

```{r, richness}
meta.df
plot_richness(loaded.ps, x="OriginalResponse", measures=c("Shannon", "Simpson")) + theme_bw()
# Warning message:
# In estimate_richness(physeq, split = TRUE, measures = measures) :
#   The data you have provided does not have
# any singletons. This is highly suspicious. Results of richness
# estimates (for example) are probably unreliable, or wrong, if you have already
# trimmed low-abundance taxa from the data.
# 
# We recommended that you find the un-trimmed data and retry.
```

## Relative Abundance Bar plot

```{r, bar-plot}
top20 <- names(sort(taxa_sums(loaded.ps), decreasing=TRUE))[1:20]
ps.top20 <- transform_sample_counts(loaded.ps, function(OTU) OTU/sum(OTU))
ps.top20 <- prune_taxa(top20, ps.top20)
plot_bar(ps.top20, fill="Family") + 
  geom_bar(stat = "identity", position = "stack", size=0) +
  facet_wrap(~OriginalResponse, scales="free_x")

# Warning message:
# Using `size` aesthetic for lines was deprecated in ggplot2 3.4.0.
# ℹ Please use `linewidth` instead.
# This warning is displayed once every 8 hours.
# Call `lifecycle::last_lifecycle_warnings()` to see where this warning was generated. 
```

This was just a bare bones demonstration of how the data from DADA2 can be easily imported into phyloseq and interrogated. For further examples on the many analyses possible with phyloseq, see [the phyloseq web site](https://joey711.github.io/phyloseq/)!

# Session Info
Always print `sessionInfo` for reproducibility!

```{r}
sessionInfo()
```

-------------------

This tutorial is based on the [Official DADA2 v1.6 Tutorial](https://raw.githubusercontent.com/benjjneb/dada2/gh-pages/tutorial_1_6.Rmd)


