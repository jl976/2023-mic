## Microbiome Overview
1. [Microbiome Overview] (lecture_slides/microbiome_overview.pdf) -- Needs update

## Microbiome Bioinformatic Analysis
1. [Bioinformatic Analysis Overview] (lecture_slides/dada2_pipeline.pdf) -- Needs update
2. [Demultiplexing](bioinformatics/00_demultiplex_bash.Rmd)
3. [DADA2: From FASTQs to OTUs](bioinformatics/01_dada2_tutorial.Rmd)

## Microbiome Statistical Analysis
1. [Statistical Analysis Overview] (lecture_slides/statistical_analysis.pdf) -- Needs update
2. [Absolute Abundance Plots](biostats/absolute_abundance_plots.Rmd)
3. [Alpha Diversity](biostats/alpha_diversity.Rmd)
4. [Relative Abundance](biostats/relative_abundance.Rmd) 
5. [Beta Diversity & Ordination](biostats/ordination.Rmd)

## Appendix
1. [Configuration File](../config.R)
2. [Download Atacama FASTQs (for demux)](prep/atacama_download.Rmd)
2. [Download FASTQs](../prep/download_matson_data.Rmd)
3. [Download Taxonomic References](prep/download_dada_references.Rmd)
4. [Decontam tutorial](prep/02_decontam_tutorial.Rmd) -- Not yet complete
5. [DADA2: From FASTQs to OTUs -- Full dataset](prep/X1_dada2_tutorial_fullDataset.Rmd)
6. [Build phylogenetic tree using ASVs -- Full dataset](prep/X2_make-phylogenetic-tree_fullDataset.Rmd)
7. [Slurm script to build the tree using RAXML](prep/run-raxml.slurm)
8. [QC phyloseq object and add tree -- Full dataset](prep/X3_analyses_fullDataset.Rmd)

