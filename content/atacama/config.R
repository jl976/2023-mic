# Common
scratch_dir="/work"
group_dir="/hpc/group/mic-2023"
data_dir=file.path(group_dir, "rawdata/atacama")

num_cpus=16

username=Sys.info()[["user"]]

# atacama_data_percent = "1"
atacama_data_percent = "10"
rawdata_dir = file.path(data_dir, paste0("atacama_", atacama_data_percent, "pct_rawdata"))
rawdata_md5sum = file.path(rawdata_dir, "md5_checksum.txt")

out_dir=file.path(scratch_dir,username,"mic2023","amplicon",paste0("atacama_", atacama_data_percent, "pct_output"))

demux_dir = file.path(out_dir, "demux")


# Files
barcode_table = file.path(out_dir,"barcodes_for_fastqmultx.tsv")
rc_barcode_table = file.path(out_dir,"rc_barcodes_for_fastqmultx.tsv")

map_file = file.path(rawdata_dir,"sample_metadata.tsv")
barcode_fastq = file.path(rawdata_dir,"barcodes.fastq.gz")
r1_fastq = file.path(rawdata_dir,"forward.fastq.gz")
r2_fastq = file.path(rawdata_dir,"reverse.fastq.gz")

dada_out_dir = file.path(out_dir, "dada2")


ps_rds = file.path(dada_out_dir, paste0("atacama_", atacama_data_percent, "pct.rds"))
ps_10pct_rds=file.path(data_dir, "atacama_10pct.rds")

ref_dir = file.path(data_dir, "dada_references")
silva_ref = file.path(ref_dir, "silva_nr_v132_train_set.fa.gz")
silva_species_ref = file.path(ref_dir, "silva_species_assignment_v132.fa.gz")
ref_md5_file = file.path(ref_dir, "md5sum.txt")
